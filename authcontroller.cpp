#include "authcontroller.h"
#include <QDebug>
#include <QObject>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QEventLoop>
#include <QString>
#include <QFile>

#include "authcontroller.h"
#include <QDebug>

/*QJsonArray Object Document
 * инкапсулируют методы для
 * 1) поиска и чтения разделов JSOn: объекты и массивы
 * 2) проверки существования и типа определенных разделов
 * для начала работы необходимо создать QJsonDocument и загрузить в него всю JSON-запись
 * а затем выделять из него массивы и объекты по их названиям (ключам)
 * QAbstractItemModel
 * класс, инкапсулирующий взаимодействие типа MVC между C++ * QML
 * 1) выдача сигналов в момент обновления структуры данных
 * чтобы движок знал, когда перестраивать графику
 * 2) соответствие строковых имен для свойств и методов c++ класса,
 * чтобы к ним мог обращаться по этим именам qml движок
 * /
*/
FriendsObject::FriendsObject(const QString &FriendId, const QString &FriendName, const QString &Photo, const QString &Status) // конструктор
    :   ob_friendid(FriendId),
        ob_friendname(FriendName),
        ob_photo(Photo),
        ob_status(Status)
{

}


QString FriendsObject::FriendId() const { // возвращает id
    return ob_friendid;
}

QString FriendsObject::FriendName() const { // возвращает имя
    return ob_friendname;
}

QString FriendsObject::Photo() const {// возвращает ссылку на фото
    return ob_photo;
}

QString FriendsObject::Status() const {// возвращает статус
    return ob_status;
}



FriendsModel::FriendsModel(QObject *parent)
{

}

void FriendsModel::addFriend(const FriendsObject &newFriend) // Добавление друга в список
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount()); // сигнал для qml, что начинается запись друга в список
    ob_friends << newFriend; // запись в список
    endInsertRows(); // сигнал в qml, что пора обновить отображение, т.к. запись окончена
}


int FriendsModel::rowCount(const QModelIndex &parent) const // возвращает размер списка друзей
{
    Q_UNUSED(parent);
    return ob_friends.count();

}

QVariant FriendsModel::data(const QModelIndex &index, int role) const // эта функция для возвращения значений переменных по вызову их ролей
{
    if (index.row() < 0 || index.row() >= ob_friends.count())
            return QVariant();
    const FriendsObject &itemToReturn = ob_friends[index.row()];
    if (role == FriendIdRole) // если вызвана роль Id друга, то возвращается Id друга и т.д.
        return itemToReturn.FriendId();
    else if (role == FriendNameRole)
        return itemToReturn.FriendName();
    else if (role == PhotoRole)
        return itemToReturn.Photo();
    else if (role == StatusRole)
        return itemToReturn.Status();

    return QVariant();


}

QHash<int, QByteArray> FriendsModel::roleNames() const // соотнесение ролей и их имен, как они будут вызываться из qml
{
    QHash<int, QByteArray> roles;
    roles[FriendIdRole] = "friendid"; // вызывать Id будем по "friendid"
    roles[FriendNameRole] = "friendname";
    roles[PhotoRole] = "photo";
    roles[StatusRole] = "status";
    return roles;
}






AuthController::AuthController(QObject *parent) : QObject(parent)
{
    na_manager = new QNetworkAccessManager();
}

void AuthController::Authentificate(QString login, QString password){ // функция авторизаци
    if (QFile(login).exists())  { // если с этого аккаунта уже заходили,
        datBase.downModel(login); // друзья загружаются из файла Базы данных
        return; // а эта функция завершается
    }
      QString clientId = "6499363";
      request.setUrl("https://oauth.vk.com/authorize" //имя метода
                      "?client_id=" + clientId //id нашего приложения
                      + "&display=mobile" //веб-старницы будут в мобильном стиле
                      "&redirect_uri=https://oauth.vk.com/blank.html" //адрес куда вы хотите перенаправляться, blank.html это значит никуда не хотите перенаправляться
                      "&scope=friends" //название ресурса которое запрашивается, в данно случае друзья (битовая маска настроек доступа приложения)
                      "&response_type=token" //что хотим в ответ, здесь это token
                      "&v=5.37" //версия API VK
                      "&state=123456");
      QEventLoop loop; //петля
      connect(na_manager, SIGNAL(finished(QNetworkReply*)), &loop, SLOT(quit()));

      reply = na_manager->get(request); // идет запрос, который выше, к вк

      loop.exec();
      response = reply->readAll();
      qDebug() << response;
      QString origin, to, ip_h, lg_h;
      QString findStr;
      int pos1, pos2;


      //Поиск ORIGIN
      findStr = "name=\"_origin\" value=\"";
      pos1 = response.indexOf(findStr) + findStr.length();
      findStr = '\"';
      pos2 = response.indexOf(findStr, pos1);

      origin = response.mid(pos1, pos2 - pos1);


      //Поиск ip_h
      findStr = "name=\"ip_h\" value=\"";
      pos1 = response.indexOf(findStr) + findStr.length();
      findStr = '\"';
      pos2 = response.indexOf(findStr, pos1);

      ip_h = response.mid(pos1, pos2 - pos1);


      //Поиск to
      findStr = "name=\"to\" value=\"";
      pos1 = response.indexOf(findStr) + findStr.length();
      findStr = '\"';
      pos2 = response.indexOf(findStr, pos1);

      to = response.mid(pos1, pos2 - pos1);


      //Поиск lg_h
      findStr = "name=\"lg_h\" value=\"";
      pos1 = response.indexOf(findStr) + findStr.length();
      findStr = '\"';
      pos2 = response.indexOf(findStr, pos1);

      lg_h = response.mid(pos1, pos2 - pos1);
      // выше идет парсинг ответа вк, в котором находятся 4 параметра, необходимые для авторизации

      qDebug() << origin << "- origin" << to << "- to" << lg_h << "- lg_h" << ip_h << "- ip_h"; // выводим для отладки

      QString authrequest = "https://login.vk.com/"
      "?act=login"
      "&soft=1"
      "&utf8=1"
      "&_origin=" + origin +
      "&lg_h=" + lg_h +
      "&ip_h=" + ip_h +
      "&to=" + to +
      "&email=" + login +
      "&pass=" + QUrl::toPercentEncoding(password);
      qDebug() << "request\t-\t" << authrequest << "\n\n\n";
      request.setUrl(authrequest);
      // дальше идет ряд запросов для полечения токена доступа
      reply = na_manager->get(request);
      loop.exec();
      response = reply->readAll();

      reply = na_manager->get(QNetworkRequest(QUrl(reply->header(QNetworkRequest::LocationHeader).toString())));
      loop.exec();
      response = reply->readAll();

      reply = na_manager->get(QNetworkRequest(QUrl(reply->header(QNetworkRequest::LocationHeader).toString())));
      loop.exec();
      response = reply->readAll();

      response = reply->header(QNetworkRequest::LocationHeader).toString();
      access_token = response.mid(response.indexOf("access_token=") + 13, response.indexOf("&expires_in") - response.indexOf("access_token=") - 13); // нахоим токен доступа
      client_id = response.mid(response.indexOf("user_id=") + 8, response.indexOf("&state=") - response.indexOf("user_id=") - 8); // получение id клиента


      request.setUrl("https://api.vk.com/method/friends.get?user_ids=" + client_id + "&fields=bdate&access_token=" + access_token + "&v=5.74"); // запрос на получения всех друзей пользователя (их айдишники)

      reply = na_manager->get(request);
      loop.exec();
      QString friends = reply->readAll();

    qDebug() << "Friends\t-\t" << friends << "\n\n\n";
      QString getFriends;
 // дальше идет вычленение друзей из тела ответа сервера вк, происходит это всё в цикле и до тех пор, пока не найден последний человек
      getFriends = "https://api.vk.com/method/users.get?user_ids=";
      pos1 = 0;
      pos2 = 0;

      while (friends.indexOf("\"id\"", pos2) != -1) { // в этом цикле считываем все айдишники друзей
          pos1 = friends.indexOf("\"id\"", pos2) + 5;
          pos2 = friends.indexOf(",", pos1);
          getFriends += (friends.mid(pos1, pos2 - pos1)) + ",";
      }

      getFriends += "&fields=photo_100,status&access_token=" + access_token + "&v=5.74"; // тут делаем запрос к вк, для получения еще и фотографий

      qDebug() << "getFriends\t-\t" << getFriends<< "\n\n\n";
      request.setUrl(getFriends);

      reply = na_manager->get(request);
      loop.exec();
      friends = reply->readAll();

      pos1 = 0;
      pos2 = 0;

      qDebug() << "Friends\t-\t" << friends << "\n\n\n";
      QString friends_ids, friends_names, friends_photos, friends_status;
      QString name, surname;
          datBase.initTable(login, friends);
          while (friends.indexOf("\"id\"", pos2) != -1) { // начало поиска друзей
              pos1 = friends.indexOf("\"id\"", pos2) + 5;
              pos2 = friends.indexOf(",", pos1);
              friends_ids = (friends.mid(pos1, pos2 - pos1));

              pos1 = friends.indexOf("\"first_name\"", pos2) + 14;
              pos2 = friends.indexOf("\"", pos1);
              name = friends.mid(pos1, pos2 - pos1);

              pos1 = friends.indexOf("\"last_name\"", pos2) + 13;
              pos2 = friends.indexOf("\"", pos1);
              surname = friends.mid(pos1, pos2 - pos1);

              friends_names = (name + " " + surname);

              pos1 = friends.indexOf("\"photo_100\"", pos2) + 13;
              pos2 = friends.indexOf("\"", pos1);
              friends_photos = (friends.mid(pos1, pos2 - pos1).replace("\\", ""));

              pos1 = friends.indexOf("\"status\"", pos2) + 10;
              pos2 = friends.indexOf("\"", pos1);
              friends_status = (friends.mid(pos1, pos2 - pos1));
              friendsModel.addFriend(FriendsObject(friends_ids, friends_names, friends_photos, friends_status)); // Добавление друга в список друзей
          }

}
