#include "messagecontroller.h"

msgObject::msgObject(const QString &msg, const bool &from) // описывается объект
    :   ob_msg(msg),
        ob_from(from)
{

}


QString msgObject::msg() const { // функция для возвращения самого сообщения
    return ob_msg;
}

bool msgObject::from() const { // функция для возвращения значения (от кого-то или кому-то)
    return ob_from;
}





msgModel::msgModel(QObject *parent) // модель сообщения , как и в auth И db controller
{

}

void msgModel::addMsg(const msgObject &newMsg)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    ob_msgs << newMsg;
    endInsertRows();
}


int msgModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return ob_msgs.count();

}

QVariant msgModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= ob_msgs.count())
            return QVariant();
    const msgObject &itemToReturn = ob_msgs[index.row()];
    if (role == msgRole)
        return itemToReturn.msg();
    else if (role == fromRole)
        return itemToReturn.from();


    return QVariant();


}

QHash<int, QByteArray> msgModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[msgRole] = "msg";
    roles[fromRole] = "from";
    return roles;
}




messagecontroller::messagecontroller(QObject *parent) : QObject(parent) // конструктор класса
{
    serv = new QTcpServer(); // создается сервер
    client_sock = new QTcpSocket(); // создается сокет

    QObject::connect(serv, SIGNAL(newConnection()), // соединяется сигнал нового подключения о слотом новое подключение
                    this, SLOT(newConnection()));

    serv->listen(QHostAddress("127.0.0.1"), 33333); // прослушка локального хоста по порту 3333
    QObject::connect(client_sock, SIGNAL(readyRead()),
                     this, SLOT(read_client_Message()));
    client_sock->connectToHost("127.0.0.1",33333); // клиент подключается к этому хосту
    qDebug() << "Server+Client init" << "\n\n";
}


void messagecontroller::newConnection()
{
    server_sock = serv->nextPendingConnection(); // перевод сокета в прослушку сервера
    connect(server_sock, SIGNAL(readyRead()),// соединение сигнала готов к чтению со слотом прочтения сокета
                     this, SLOT(read_serv_Message()));
}

void messagecontroller::sendMessage(QString message) { // функция посылки сообщения
    QByteArray data; // массив данных для передачи
    data.append(message);// добавляем в него наше сообщение
    server_sock->write(data);  // пишем в сокет сервера
    server_sock->waitForBytesWritten();
    client_sock->write(data); // и сразу же в сокет клиента
    client_sock->waitForBytesWritten();
}

void messagecontroller::read_client_Message() // как только данные записаны в сокет клиента
{
    QString msg = client_sock->readAll(); // данные считвыаются
    messages.addMsg(msgObject(msg, 1)); // добавляются в модель
}

void messagecontroller::read_serv_Message() // как только данные записаны в сокет сервера
{
    QString msg = server_sock->readAll();// данные считвыаются
    messages.addMsg(msgObject(msg, 0));// добавляются в модель
}
